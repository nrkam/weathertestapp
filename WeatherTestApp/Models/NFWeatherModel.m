//
//  NFWeatherDetailsModel.m
//  WeatherTestApp
//
//  Created by Narek Fidanyan on 12.10.17.
//  Copyright © 2017 Narek Fidanyan. All rights reserved.
//

#import "NFWeatherModel.h"

@implementation NFWeatherModel

+ (NSString *)primaryKey {
    return @"rowID";
}

//+ (NSArray *)ignoredProperties {
//    return @[@"tempretureFluctation"];
//}

@end
