//
//  NFWeatherDetailsModel.h
//  WeatherTestApp
//
//  Created by Narek Fidanyan on 12.10.17.
//  Copyright © 2017 Narek Fidanyan. All rights reserved.
//

#import <Realm/Realm.h>

@interface NFWeatherModel : RLMObject

@property (nonatomic, retain) NSNumber <RLMInt> *rowID;
@property (nonatomic, retain) NSString *cityName;
@property (nonatomic, retain) NSDate *tempretureDate;
@property (nonatomic, retain) NSNumber <RLMFloat> *tempretureValue;

// Ignored (for internal usage)
@property (nonatomic, retain) NSNumber <RLMFloat> *tempretureFluctation;

@end
