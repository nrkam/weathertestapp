//
//  WeatherDetailsTVCell.h
//  WeatherTestApp
//
//  Created by Narek Fidanyan on 12.10.17.
//  Copyright © 2017 Narek Fidanyan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NFWeatherModel;

@interface WeatherDetailsTVCell : UITableViewCell

- (void)setData:(NFWeatherModel *)weatherData;

@end
