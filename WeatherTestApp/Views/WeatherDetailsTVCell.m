//
//  WeatherDetailsTVCell.m
//  WeatherTestApp
//
//  Created by Narek Fidanyan on 12.10.17.
//  Copyright © 2017 Narek Fidanyan. All rights reserved.
//

#import "WeatherDetailsTVCell.h"
#import "NFWeatherModel.h"

#define kArrowUp    @"\u25b4"
#define kArrowDown  @"\u25be"

#define RGBAColor(r, g, b, a) [UIColor colorWithRed:r / 255.0 green:g / 255.0 blue:b / 255.0 alpha:a]

@interface WeatherDetailsTVCell ()

@property (weak, nonatomic) IBOutlet UILabel *mainValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *fluctuationLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end

@implementation WeatherDetailsTVCell

- (void)setData:(NFWeatherModel *)weatherData {
    // Set tempreture in Celsius
    CGFloat cTemp = ([weatherData.tempretureValue floatValue] - 32) * 5/9;
    self.mainValueLabel.text = [NSString stringWithFormat:@"%.0f °C", cTemp];
    
    // Set date
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"d MMMM  HH:mm:ss"];
    self.dateLabel.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:weatherData.tempretureDate]];
    
    // Set fluctuation
    NSNumber *fluc = weatherData.tempretureFluctation;
    NSMutableAttributedString *attrString;
    if (fluc) {
        
        BOOL isPositive = (fluc.floatValue >= 0);
        if (!isPositive) {
            fluc = @(-fluc.floatValue);
        }
        
        NSString *arrowSign = isPositive ? kArrowUp : kArrowDown;
        UIColor *color = isPositive ? RGBAColor(108, 191, 17, 1) : RGBAColor(255, 83, 94, 1);
        
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setNumberStyle:NSNumberFormatterNoStyle];
        [formatter setMinimumFractionDigits:0];
        [formatter setMaximumFractionDigits:0];
        
        NSString *flucValue = [NSString stringWithFormat:@"%@%@%%", arrowSign, [formatter stringFromNumber:fluc]];
        
        NSString *flucString = [NSString stringWithFormat:@"Fluc %@", flucValue];
        attrString = [[NSMutableAttributedString alloc] initWithString:flucString];
        
        NSDictionary *attrDict = @{
                                   NSForegroundColorAttributeName : color,
                                   NSFontAttributeName : [UIFont systemFontOfSize:16 weight:UIFontWeightSemibold]
                                   };
        [attrString addAttributes:attrDict range:[flucString rangeOfString:flucValue]];
        
    } else {
        
        attrString = [[NSMutableAttributedString alloc] initWithString:@"N/A"];
        
    }
    
    self.fluctuationLabel.attributedText = attrString;
}


@end
