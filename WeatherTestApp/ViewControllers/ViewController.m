//
//  ViewController.m
//  WeatherTestApp
//
//  Created by Narek Fidanyan on 27.9.17.
//  Copyright © 2017 Narek Fidanyan. All rights reserved.
//

#import "ViewController.h"
#import "DetailesViewController.h"

#import "CityModel.h"
#import "NetworkManager.h"
#import "NFDatabaseManager.h"
#import "NFWeatherModel.h"

@interface ViewController () <UITextViewDelegate, UITableViewDelegate, UITableViewDataSource> {
    CGFloat textViewSmallModeMultiplier;
    CGFloat textViewBigModeMultiplier;
    
    BOOL isTextViewBig;
    
    CityModel *tempCityForDequeue;
}

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightConstraint;

@property (strong, nonatomic) NSArray <CityModel *>*citiesDataSource;
@property (strong, nonatomic) CityModel *selectedCity;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Init some vars
    textViewSmallModeMultiplier = 0.33f;
    textViewBigModeMultiplier = 0.66f;
    
    self.citiesDataSource = [self parseDataFromJSON];
    [self.tableView reloadData];
    
    [self makeTextViewBigger:NO];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource and related
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.citiesDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *indentifier = @"CityTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
    }
    
    tempCityForDequeue = self.citiesDataSource[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@, %@", tempCityForDequeue.name, tempCityForDequeue.countryCode];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.selectedCity = self.citiesDataSource[indexPath.row];
    
    [self displayCityInfoForSelectedCity];
    
    // Get weather from net
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] getWeatherForCity:self.selectedCity withCompletion:^(id responseDict, NSData *responseData, NSError *error) {
        __strong typeof(self)strongSelf = weakSelf;
        
        NSNumber *tempNumberValue = responseDict[@"main"][@"temp"];
       
        // Save in DB
        NSDate *weatherDate = [NSDate date];
        NFWeatherModel *weatherModel = [NFWeatherModel new];
        
        weatherModel.rowID = @((NSUInteger)floor([weatherDate timeIntervalSince1970]));
        weatherModel.cityName = strongSelf.selectedCity.name;
        weatherModel.tempretureDate = weatherDate;
        weatherModel.tempretureValue = tempNumberValue;
        [[NFDatabaseManager sharedInstance] addWeatherDetails:weatherModel];
        
        //
        DetailesViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailesViewController"];
        vc.weatherDataSource = [strongSelf getWeatherFor:strongSelf.selectedCity];
        [strongSelf.navigationController pushViewController:vc animated:YES];
    }];
}

- (RLMResults *)getWeatherFor:(CityModel *)city {
    RLMResults *results = [NFWeatherModel objectsWhere:@"cityName == %@", city.name];
    return results;
}

- (void)displayCityInfoForSelectedCity {
    NSString *textViewText = [NSString stringWithFormat:@"%@, %@\n\n%@", self.selectedCity.name, self.selectedCity.countryCode, self.selectedCity.desc];
    
    self.textView.text = textViewText;
}

#pragma mark - UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    [self makeTextViewBigger:!isTextViewBig];
    
    return NO;
}

#pragma mark - Animation
- (void)makeTextViewBigger:(BOOL)bigger {
    self.textView.userInteractionEnabled = NO;
    
    CGFloat multiplier = bigger ? textViewBigModeMultiplier : textViewSmallModeMultiplier;
    self.textViewHeightConstraint.constant = multiplier * self.view.frame.size.height;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.view layoutIfNeeded];
    }
                     completion:^(BOOL finished) {
                         isTextViewBig = bigger;
                         self.textView.userInteractionEnabled = YES;
    }];
}

#pragma mark - Other methods
- (NSArray *)parseDataFromJSON {
    NSArray *tempArray = [NSArray new];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"cities" ofType:@"txt"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSDictionary *citiesDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    tempArray = [CityModel arrayOfModelsFromDictionaries:citiesDict[@"cities"] error:nil];
    
    return tempArray;
}

@end
