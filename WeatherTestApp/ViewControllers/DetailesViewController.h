//
//  DetailesViewController.h
//  WeatherTestApp
//
//  Created by Narek Fidanyan on 12.10.17.
//  Copyright © 2017 Narek Fidanyan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NFCityRealmModel;
@class RLMResults;

@interface DetailesViewController : UIViewController

@property (strong, nonatomic) RLMResults *weatherDataSource;

@end
