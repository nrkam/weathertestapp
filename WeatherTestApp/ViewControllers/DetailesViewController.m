//
//  DetailesViewController.m
//  WeatherTestApp
//
//  Created by Narek Fidanyan on 12.10.17.
//  Copyright © 2017 Narek Fidanyan. All rights reserved.
//

#import "DetailesViewController.h"
#import "WeatherDetailsTVCell.h"
#import "NFWeatherModel.h"

#import "NFDatabaseManager.h"
#import "RLMResults.h"

@interface DetailesViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation DetailesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // *** Comment this if dont want Simulated fluctuation
    NFWeatherModel *model = self.weatherDataSource.lastObject;
    [[RLMRealm defaultRealm] beginWriteTransaction];
    model.tempretureValue = @(arc4random_uniform(30) + [model.tempretureValue floatValue]);
    [[RLMRealm defaultRealm] commitWriteTransaction];
    
    // Calculate fluctuation
    if (self.weatherDataSource.count == 1) {
        return; // Not enough data for fluctation
    }
    
    // ...continue then
    for (int i = 0; i < self.weatherDataSource.count; i++) {
        if (i >= 1) {
            NFWeatherModel *current = self.weatherDataSource[i];
            NFWeatherModel *previous = self.weatherDataSource[i - 1];
            
            [[RLMRealm defaultRealm] beginWriteTransaction];
            current.tempretureFluctation = @([current.tempretureValue floatValue] - [previous.tempretureValue floatValue]);
            [[RLMRealm defaultRealm] commitWriteTransaction];
        }
    }
    
    // Reverse array order
    RLMSortDescriptor *desc = [RLMSortDescriptor sortDescriptorWithKeyPath:@"tempretureDate" ascending:NO];
    self.weatherDataSource = [self.weatherDataSource sortedResultsUsingDescriptors:@[desc]];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource and related
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.weatherDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *indentifier = @"WeatherDetailsTVCell";
    WeatherDetailsTVCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if (!cell) {
        cell = [[WeatherDetailsTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
    }
    
    NFWeatherModel *model = (NFWeatherModel *)self.weatherDataSource[indexPath.row];
    [cell setData:model];
    
    return cell;
}

@end
